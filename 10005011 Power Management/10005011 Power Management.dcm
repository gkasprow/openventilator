EESchema-DOCLIB  Version 2.0
#
$CMP +24VUTIL
D Power symbol creates a global label with name "+24VUTIL"
$ENDCMP
#
$CMP +3.0VREF
D Power symbol creates a global label with name "+3.0VREF"
K power-flag
$ENDCMP
#
$CMP +3.3V
D Power symbol creates a global label with name "+3.3V"
K power-flag
$ENDCMP
#
$CMP +5VPIC
D Power symbol creates a global label with name "+5VPIC"
K power-flag
$ENDCMP
#
$CMP 0VANA
D Power symbol creates a global label with name "0VANA" , analog ground
K power-flag
$ENDCMP
#
$CMP 1N5819
D DIODE SCHOTTKY 40V 1A SOD123
K diode Schottky
F https://www.diodes.com/assets/Datasheets/ds30217.pdf
$ENDCMP
#
$CMP 2N7002
D N-channel MOSFET, 60V Vds, 200 mA Id, SOT23, AEC-Q101, Diodes Incorporated
K N-Channel MOSFET
F https://www.diodes.com/assets/Datasheets/2N7002AQ.pdf
$ENDCMP
#
$CMP 47pF_array
D Unpolarized capacitor, small symbol
K capacitor cap
F https://media.digikey.com/pdf/Data%20Sheets/Kemet%20PDFs/COG_Dielectric_Rev_Mar_2015.pdf
$ENDCMP
#
$CMP BAT54
D single schottky barrier diode, common cathode
K schottky diode common cathode
F http://www.onsemi.com/pub/Collateral/BAT54LT1-D.PDF
$ENDCMP
#
$CMP BAT54C
D dual schottky barrier diode, common cathode
K schottky diode common cathode
F https://media.digikey.com/pdf/Data%20Sheets/ON%20Semiconductor%20PDFs/BAT54(A,C,S).pdf
$ENDCMP
#
$CMP BZX84C4V7
D zener diode, 350mW, 4.7V
K zener diode
F https://4donline.ihs.com/images/VipMasterIC/IC/ONSM/ONSM-S-A0003405162/ONSM-S-A0003405162-1.pdf?hkey=EC6BD57738AE6E33B588C5F9AD3CEFA7
$ENDCMP
#
$CMP BZX84C5V1
D zener diode, 350mW, 5.1V
K 10022657
F https://www.diodes.com/assets/Datasheets/ds18001.pdf
$ENDCMP
#
$CMP CMF2
D Power entry module without line filter
K power entry module switch
F https://www.schurter.com/en/datasheet/typ_CMF2__CMF5.pdf
$ENDCMP
#
$CMP CP
D Polarized capacitor
K cap capacitor
F ~
$ENDCMP
#
$CMP CY7C1021DV33
D IC,RAM,3V3,64KX16,10NS,TSOP
K 10022899
F https://www.cypress.com/file/42751/download
$ENDCMP
#
$CMP CY7C67300-100AXI
D IC,MICROCONTROLLER,TQFP100
K 10022898
F https://www.cypress.com/file/39146/download
$ENDCMP
#
$CMP Conn_01x06_Female_x02_SidePads
D Generic connector, single row, 01x06, script generated (kicad-library-utils/schlib/autogen/connector/)
K connector
F ~
$ENDCMP
#
$CMP DLP31SN551ML2L
D COMMON MODE CHOKE COIL 550OHM
K 10039588
F https://www.murata.com/en-eu/products/productdata/8796756410398/EFLE0007.pdf
$ENDCMP
#
$CMP DS2480B
D Serial to 1-Wire Line Driver
K 1-Wire 1wire uart serial
F https://datasheets.maximintegrated.com/en/ds/DS2480B.pdf
$ENDCMP
#
$CMP ECM100US33>2413
D POWER SUPPLY, 220VAC, 33VDC, 100W
K 10022932
$ENDCMP
#
$CMP GND1
D Power symbol creates a global label with name "GND1"
K power-flag
$ENDCMP
#
$CMP INA170
D High-Side, Bidirectional Current-Sense Amplifier
K current monitor shunt sensor bidirectional high
F https://www.ti.com/lit/ds/symlink/ina170.pdf
$ENDCMP
#
$CMP IRFR024NTRPBF
D 17A Id, 55V Vds, 75mOhm Rds, N-Channel MOSFET, DPAK (TO-252AA)
K N-Channel MOSFET
F https://www.infineon.com/dgdl/irfr024npbf.pdf?fileId=5546d462533600a40153562cf721203c
$ENDCMP
#
$CMP IRLML2803TRPBF
D 1.2A Id, 30V Vds, 250mOhm Rds, N-Channel MOSFET, SOT23
K N-Channel MOSFET
F https://www.infineon.com/dgdl/irlml2803pbf.pdf?fileId=5546d462533600a4015356682aff260f
$ENDCMP
#
$CMP IRLML5103TRPBF
D -0.76A Id, -30V Vds, 0.60Ohm Rds, P-Channel MOSFET, SOT-23
K P-Channel MOSFET
F https://www.infineon.com/dgdl/irlml5103pbf.pdf?fileId=5546d462533600a401535668505d2617
$ENDCMP
#
$CMP LM4040DIM3-5.0
D 2.048V Precision Micropower Shunt Voltage Reference, SOT-23
K diode device voltage reference shunt
F http://www.ti.com/lit/ds/symlink/lm4040-n.pdf
$ENDCMP
#
$CMP LM4120AIM5-3.0
D IC,VOLTAGE REFERENCE,5MA,3V +/-2%,SOT23
K 10022896
F http://www.ti.com/lit/ds/symlink/lm4120.pdf
$ENDCMP
#
$CMP LT1620CMS8
F https://www.analog.com/media/en/technical-documentation/data-sheets/16201f.pdf
$ENDCMP
#
$CMP LT1716IS5
D Single 1.8V Low-Power Push-Pull Output Comparator, SOT-23-5
K single comparator
F https://www.analog.com/media/en/technical-documentation/data-sheets/LT1716.pdf
$ENDCMP
#
$CMP LTC1435CS
D High Efficiency Low Noise Synchronous Step-Down Switching Regulator, SO-16
K buck controller 36V
F https://www.analog.com/media/en/technical-documentation/data-sheets/1435f.pdf
$ENDCMP
#
$CMP LTC3780
F https://www.analog.com/media/en/technical-documentation/data-sheets/LTC3780.pdf
$ENDCMP
#
$CMP MCDM(R)-02-T
D 2x DIP Switch, Single Pole Single Throw (SPST) switch, small symbol
K dip switch
F ~
$ENDCMP
#
$CMP NC7SZ66
D IC,BUS SWITCH,SC70
K 10039588
F https://www.onsemi.com/pub/Collateral/NC7SZ66-D.pdf
$ENDCMP
#
$CMP PGND
D Power symbol creates a global label with name "PGND" , ground
K power-flag
$ENDCMP
#
$CMP PGND1
D Power symbol creates a global label with name "PGND1"
K power-flag
$ENDCMP
#
$CMP PIC18F6622
D PIC18F nanoWatt Technology MCU, 40MHz, 64KB Flash, 3.84KB RAM, 1KB EEPROM, 2.0-5.5V, 54 GPIO, TQFP-64
K Flash Based 8-Bit Microcontroller
F http://ww1.microchip.com/downloads/en/DeviceDoc/39646c.pdf
$ENDCMP
#
$CMP PMEG4005ET
D 0.5A Schottky Diode, SOT-23
K diode Schottky
F https://assets.nexperia.com/documents/data-sheet/PMEGXX05ET_SER.pdf
$ENDCMP
#
$CMP SI7884BDP
D 58A Id, 40V Vds, 7.5mOhm Rds, N-Channel MOSFET, PowerPAK_SO-8
K N-Channel MOSFET
F http://www.vishay.com/docs/68395/si7884bd.pdf
$ENDCMP
#
$CMP SN74LVC2T45
D IC,RAM,3V3,64KX16,10NS,TSOP
K Level-Shifter CMOS-TTL-Translation
F http://www.ti.com/lit/ds/symlink/sn74lvc2t45.pdf
$ENDCMP
#
$CMP STPS340U
D DIODE SCHOTTKY 40V 1A SOD123
K diode Schottky
F http://www.st.com/content/ccc/resource/technical/document/datasheet/bb/db/21/0c/04/d9/41/a5/CD00000844.pdf/files/CD00000844.pdf/jcr:content/translations/en.CD00000844.pdf
$ENDCMP
#
$CMP SUD50P06-15L-E3
D 50A Id, 60V Vds, P-Channel Power MOSFET, 20mOhm Ron @ -10V, 110nC, 160nC max. Qg, -55 to 175 °C, TO-252-2
K P-Channel Power MOSFET
F http://www.vishay.com/docs/72250/sud50p06.pdf
$ENDCMP
#
$CMP SZ1SMB
D transient-voltage-suppression (TVS), 36V, 5A, unidirectional
K diode TVS transient voltage supressor SMB
F https://www.littelfuse.com/~/media/electronics/datasheets/tvs_diodes/littelfuse_tvs_diode_sz1smb_datasheet.pdf.pdf
$ENDCMP
#
$CMP TLV3012AIDBV
D Nanopower 1.8V comparator with voltage reference
K comparator
F http://www.ti.com/lit/ds/symlink/tlv3012.pdf
$ENDCMP
#
$CMP TLV7031DBV
D Single, 1.6V-6.5V, 315nA Quiescent, Push-Pull Output, Comparator, SOT-23-5/SC-70
K single comparator
F https://www.ti.com/lit/ds/symlink/tlv7031.pdf
$ENDCMP
#
$CMP TestPoint
D test point
K test point tp
F ~
$ENDCMP
#
$CMP USB_A_4shield
D USB Type A connector
K connector USB
F https://www.molex.com/pdm_docs/sd/673298001_sd.pdf
$ENDCMP
#
$CMP USB_B_Mini_2shield
D USB MINI B
K 10022867
F https://www.molex.com/webdocs/datasheets/pdf/en-us/0548190519_IO_CONNECTORS.pdf
$ENDCMP
#
$CMP VACDC
D Power symbol creates a global label with name "VACDC"
K power-flag
$ENDCMP
#
$CMP VALIM
D Power symbol creates a global label with name "VALIM"
K power-flag
$ENDCMP
#
$CMP VALIM_PERM
D Power symbol creates a global label with name "VALIM_PERM"
K power-flag
$ENDCMP
#
$CMP VBAT
D Power symbol creates a global label with name "VBAT"
K power-flag
$ENDCMP
#
$CMP VDCEXT
D Power symbol creates a global label with name "VDCEXT"
K power-flag
$ENDCMP
#
#End Doc Library
